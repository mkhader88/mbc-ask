import React, { Component } from 'react'

class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userName: '',
      password: ''
    }
  }

  render() {
    return (
      <form
        className="login-form"
        action="."
        onSubmit={e => {
          e.preventDefault()
          this.props.onSubmitLoginForm(this.state.userName, this.state.password)
          this.setState({ userName: '', password: '' })
        }}
      >
        <div className="login-field-container">
          {/* <label className="login-label" htmlFor="ask">User Name</label> */}
          <input
            type="text"
            id={'userName'}
            placeholder={'Enter User Name...'}
            value={this.state.userName}
            onChange={e => this.setState({ userName: e.target.value })}
          />
        </div>
        <div className="login-field-container">
          {/* <label className="login-label" htmlFor="ask">Password</label> */}
          <input
            type="password"
            id={'password'}
            placeholder={'Enter Password...'}
            value={this.state.password}
            onChange={e => this.setState({ password: e.target.value })}
          />
        </div>
        <div className="submit-login">
          <input type="submit" value={'Login'} />
        </div>
      </form>
    )
  }
}

export default LoginForm
